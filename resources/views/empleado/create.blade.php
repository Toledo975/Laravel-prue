<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <button onclick="getLocation()">localizacion</button>

    <h1 id="title" class="text-center">Lista Empleados</h1>

    <div class="form-wrap">
        

        <table class="table table-dark table-striped" class="table table-ligth">
            <thead class="thead-ligth">
                <tr>
                    <th>#</th>
                    <th>Foto</th>
                    <th>Nombre</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Geololicacion                      </th>
                    <th>Correo</th>
                    <th>Acciones</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($empleados as $empleado)
                <tr>
                    <td>{{ $empleado->id}}</td>
                    <td>
                        <img class="imgs" src="{{ asset('storage'.'/'.$empleado->Foto)}}" alt="">

                    </td>
                    <td>{{ $empleado->Nombre}}</td>
                    <td>{{ $empleado->ApellidoPaterno}}</td>
                    <td>{{ $empleado->ApellidoMaterno}}</td>
                    <td> <p id="demo"></p>
                        {{ $empleado->geo}}
                    </td>
                    <td>{{ $empleado->Correo}}</td>
                    <td>


                        <a href="{{ url('/empleado/'.$empleado->id.'/edit')}}">
                            Editar
                        </a>


                        <form action="{{ url('/empleado/'.$empleado->id)}}" method="post">
                            @csrf

                            {{method_field('DELETE')}}
                            <input class="btn btn-danger" type="submit"
                                onclick="return confirm('¿Quieres borrar este empleado?')" value="borrar">

                        </form>


                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    


    <script>
        var x = document.getElementById("demo");
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
            function showPosition(position) {
             x.innerHTML = "Latitude y longitud: " + position.coords.latitude + 
                "," + position.coords.longitude;
        }


        function creargeo(){
            var dato = new formData(document.getElementById('creargeo'));
            fetch('empleados',{
                method: 'get',
                body: data
            }).then(function (d){
                console.log(d);
            });
        }
    </script>


    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap');

        .form-label {
            font-family: 'Roboto', sans-serif;
            font-size: 20px;
        }

        .container {
            max-width: 1230px;
            width: 100%;
        }

        h1 {
            font-weight: 700;
            font-size: 45px;
            font-family: 'Roboto', sans-serif;
        }

        .form-control {
            height: 50px;
            background: #ecf0f4;
            border-color: transparent;
            padding: 0 15px;
            font-size: 16px;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        .form-wrap {
            background: rgba(255, 255, 255, 1);
            width: 100%;
            max-width: 70%;
            padding: 50px;
            margin: 0 auto;
            position: relative;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            -webkit-box-shadow: 0px 0px 40px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 0px 0px 40px rgba(0, 0, 0, 0.15);
            box-shadow: 0px 0px 40px rgba(0, 0, 0, 0.15);
        }

        .imgs {
            width: 250px;
            padding: 10px;
        }
    </style>





</body>

</html>
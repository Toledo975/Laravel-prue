<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="mb-3">
                <label for="Nombre" class="form-label">Nombre</label>
                <input type="text" name="Nombre" id="Nombre" value="{{ (isset($empleado))? $empleado->Nombre:''}}" placeholder="Enter your name"
                    class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="mb-3">
                <label for="ApellidoPaterno" class="form-label">Apellido Paterno</label>
                <input type="text" name="ApellidoPaterno" id="ApellidoPaterno" value="{{ (isset($empleado))? $empleado->ApellidoPaterno:''}}" placeholder="Enter your last name"
                    class="form-control">
                   
            </div>
        </div>
    </div>
</div>


{{-- segunda columna --}}


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            
            <div class="mb-3">
                <label for="ApellidoMaterno" class="form-label">Apellido Materno</label>
                <input type="text" name="ApellidoMaterno" id="ApellidoMaterno" value="{{ (isset($empleado))? $empleado->ApellidoMaterno: ''}}" placeholder="Enter your last name"
                class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            
            <div class="mb-3">
                <label for="Correo" class="form-label">Correo</label>
                <input type="email" name="Correo" id="Correo" value="{{ (isset($empleado))? $empleado->Correo: ''}}" placeholder="Escrie tu email"
                class="form-control">
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            
            <div class="mb-3">
                <label for="Foto" class="form-label">Foto</label>
                <br>
                {{ (isset($empleado))? $empleado->Foto: ''}}
                
                <img class="imgs" src="{{ (isset($empleado))? asset('storage'.'/'.$empleado->Foto):''}}" alt="">    
            
                <input type="file" name="Foto" value="" id="Foto">
            </div>
        </div>
    </div>
</div>   

<div class="mb-3">
    <input class="btn btn-success text-center" type="submit" value="Actualizar">
</div>


<style>

.imgs{
    width: 300px;
    
    margin-left: 250px;
}

</style>
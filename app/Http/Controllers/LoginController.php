<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Cache\RateLimiting\Limit;

class LoginController extends Controller
{
    //

    public function show()
    {
        // if(Auth::check()){
        //     return redirect('/home');
        
        // }
        return view('login');
    }   

    public function login(LoginRequest $request )
    {
        
        
        $credentials = $request->getCredentials();
        
        if(!Auth::validate($credentials )){
            return redirect()->to('login')->withErrors(trans('auth.failed'));
            
        }
        
        
        $user = Auth::getProvider()->retrieveByCredentials($credentials); 
        // sirve para ver el debug del programa
        //dd($user);

        Auth::login($user);

        return $this->authenticated($request, $user);
    }

    public function authenticated(Request $request, $user) 
    {
        return redirect('home');
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('login', function (Request $request) {
            return Limit::perMinute(3);
            
        });
    }




    

}
